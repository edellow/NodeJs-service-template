# < Application/Service Name >

## Description

< Provide a summary of the intention of the application/service. Why does this service exist? What's implemented? >

## Installation

### Mac Instructions

< Provide a list of any major dependencies that need to be installed locally prior to OS-specific instructions >

< Provide step-by-step instructions on getting the application set up and running locally for Mac users >

< Note: this should include any local DB setup >

### Windows Instructions

< Provide a list of any major dependencies that need to be installed locally prior to OS-specific instructions >

< Provide step-by-step instructions on getting the application set up and running locally for Windows users >

< Note: this should include any local DB setup >

### Docker Instructions (optional)

< Provide a list of any major dependencies that need to be installed locally prior to OS-specific instructions >

< Ideally, our apps should also be containerized via Docker to allow for production equivalence between developer operating systems and production deployment >

## Usage

< List the package.json scripts and their functions to show how to do common actions with the application >

## Configuration

< List any configuration steps, including ENV variables, database values, that affect configuration of the system >

Developer Local Instance Configurations

### Environment Variables

Create a .env file at the root of this application and set the following required values to get started:

```
# The node_env must be either 'production', 'test' or 'development', if left empty then defaults to development
NODE_ENV=

# The port that this service is running on. Defaults to 3000.
PORT=

# A flag to indicate if the service should run in debug mode. Defaults to false.
DEBUG_MODE=

# (REQUIRED) following are required for sequelize to initiate the DB in postgres.
DATABASE_URL=
TEST_DATABASE_URL=

# A flag indicating if SSL should be enforced for database connections. This should be false for local database instances. Defaults to false.
DATABASE_USE_SSL=

# The hostname for the Swagger UI. Defaults to '' which is a top directory.
SWAGGER_HOSTNAME=

# A list of hosts allowed to access this service. Use http://localhost for local development. Defaults to ''.
CORS_ORIGINS_WHITELIST=

# CORS_ORIGINS_MATCH variable (optional) allows additional CORS whitelisted origins
# to be added that match the given regex pattern. Defaults to ''.
# Example: CORS_ORIGINS_MATCH=(https|http)://(ccm-).+(-feature-).+(.herokuapp.com)
# matches https://ccm-myservice-feature-76sf78965.herokuapp.com or
# https://ccm-new-service-2-feature-4ddfg7633.herokuapp.com/
CORS_ORIGINS_MATCH=

# (REQUIRED) The secret key for signing and decoding a user access token. This needs to be equivalent to the token secret in ccm-core service.
ACCESS_TOKEN_SECRET=

# (REQUIRED) following are required for creating the Axios connection to CCM Core API
# ex. https://ccm-core-development.herokuapp.com/api/v1
CCM_CORE_API_BASE_URL=

# In development, if newrelic is NOT installed on the local machine, add these two environment variables to
# stop errors in the console during startup and tests
NEW_RELIC_ENABLED=false
NEW_RELIC_NO_CONFIG_FILE=true


## Continuous Integration

< Explain the contents of the .gitlab-ci.yml file - what tasks are performed for different pipeline stages >
```

/* eslint-disable no-process-env */ // Allow process.env to be used in this class ONLY
import dotenv from 'dotenv';
import { isEmpty } from 'lodash';

dotenv.config();

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const getEnvVar = (envVar = '', defaultValue?: any): string => {
  if (envVar) {
    return envVar;
  }
  return defaultValue;
};

// eslint-disable-next-line @typescript-eslint/no-extraneous-class
export class Env {
  static get nodeEnv(): string {
    const val = getEnvVar(process.env.NODE_ENV, 'production');

    if (val !== 'development' && val !== 'test' && val !== 'production') {
      throw new Error('Wrong value passed for NODE_ENV.');
    }

    return val;
  }

  static get port(): string {
    return getEnvVar(process.env.PORT, '3000');
  }

  static get debugMode(): boolean {
    const val = getEnvVar(process.env.DEBUG_MODE, false);

    if (typeof val === 'string' && val === 'true') {
      return true;
    }
    return false;
  }

  static get databaseURL(): string {
    let val = '';

    if (Env.nodeEnv === 'test') {
      val = getEnvVar(process.env.TEST_DATABASE_URL, '');
      if (isEmpty(val)) {
        throw new Error('Missing environment variable for TEST_DATABASE_URL');
      }
    } else {
      val = getEnvVar(process.env.DATABASE_URL, '');
      if (isEmpty(val)) {
        throw new Error('Missing environment variable for DATABASE_URL');
      }
    }
    return val;
  }

  static get databaseUseSSL(): boolean {
    const val = getEnvVar(process.env.DATABASE_USE_SSL, true);

    if (isEmpty(val)) {
      throw new Error('Missing environment variable for DATABASE_USE_SSL');
    }

    if (typeof val === 'string' && val === 'true') {
      return true;
    }
    return true;
  }

  static get swaggerHostname(): string {
    const val = getEnvVar(process.env.SWAGGER_HOSTNAME, '');

    return val;
  }

  static get corsOrigins(): string {
    return getEnvVar(process.env.CORS_ORIGINS_WHITELIST, '');
  }

  static get corsOriginsMatch(): string {
    return getEnvVar(process.env.CORS_ORIGINS_MATCH, '');
  }

  static get accessTokenSecret(): string {
    const val = getEnvVar(process.env.ACCESS_TOKEN_SECRET, '');

    if (isEmpty(val)) {
      throw new Error('Missing environment variable for ACCESS_TOKEN_SECRET');
    }
    return val;
  }
}

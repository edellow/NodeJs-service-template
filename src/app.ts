import express from 'express';
import { configureCORS } from './cors';
import { errorLoggerMiddleware, loggerMiddleware } from './newRelic';
import { configureRoutes } from './routes';

// import createSwaggerServices from './docs_openapi/swaggerService';
const app = express();

// Configure CORS
configureCORS(app);

// Configure NewRelic
app.use(loggerMiddleware);
app.use(errorLoggerMiddleware);

configureRoutes(app);

export default app;

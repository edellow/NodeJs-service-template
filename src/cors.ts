import cors, { CorsOptions } from 'cors';
import { Application, Request } from 'express';
import { Env } from './env';

const whitelist = Env.corsOrigins.split(',');
const originMatchRegex = Env.corsOriginsMatch;
export const configureCORS = (app: Application): void => {
  const corsOptions: CorsOptions = {
    origin: (
      requestOrigin: string | undefined,
      callback: (err: Error | null, allow?: boolean) => void,
    ) => {
      if (
        requestOrigin === '' ||
        requestOrigin === undefined ||
        whitelist.includes(requestOrigin)
      ) {
        callback(null, true);
      } else if (originMatchRegex && requestOrigin.match(new RegExp(originMatchRegex, 'gi'))) {
        callback(null, true);
      } else {
        callback(new Error('Not allowed by CORS'));
      }
    },
    optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
  };
  app.options('*', cors<Request>(corsOptions)); // enable cors pre-flight
  app.use(cors(corsOptions));
};

import { addService, divideService, multiplyService, subtractService } from './mathService';

describe('math services', () => {
  // Add
  test('add should return 99.9', async () => {
    const response = await addService({ value1: 90, value2: 9.9 });
    expect(response).toBe(99.9);
  });

  // Subtract
  test('subtract should return 80.1', async () => {
    const response = await subtractService({ value1: 90, value2: 9.9 });
    expect(response).toBe(80.1);
  });

  // Divide
  test('divide should return 10', async () => {
    const response = await divideService({ value1: 90, value2: 9 });
    expect(response).toBe(10);
  });

  // Multiply
  test('multiply should return 990', async () => {
    const response = await multiplyService({ value1: 90, value2: 9 });
    expect(response).toBe(810);
  });
});

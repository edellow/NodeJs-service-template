export const pingService = async (): Promise<string> => {
  try {
    return 'pong';
  } catch (e: unknown) {
    return Promise.reject(e);
  }
};

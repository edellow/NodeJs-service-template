import { MathParams } from '../models/mathModels';

export const addService = async (mathParams: MathParams): Promise<number> => {
  try {
    return mathParams.value1 + mathParams.value2;
  } catch (e: unknown) {
    return Promise.reject(e);
  }
};

export const subtractService = async (mathParams: MathParams): Promise<number> => {
  try {
    return mathParams.value1 - mathParams.value2;
  } catch (e: unknown) {
    return Promise.reject(e);
  }
};

export const multiplyService = async (mathParams: MathParams): Promise<number> => {
  try {
    return mathParams.value1 * mathParams.value2;
  } catch (e: unknown) {
    return Promise.reject(e);
  }
};

export const divideService = async (mathParams: MathParams): Promise<number> => {
  try {
    return mathParams.value1 / mathParams.value2;
  } catch (e: unknown) {
    return Promise.reject(e);
  }
};

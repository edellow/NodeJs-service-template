/* eslint-disable no-console */
import http from 'http';
import app from './app';
import { Env } from './env';

const asciiLogo = `
              .:  :.
              ::..::
         ::   ::  ::   ::
        ::::  ::  ::  ::::
         ::  .::  ::.  ::
     .::::::::.:  :.::::::::.
       ::..            ..::
     .::::::....  ....::::::.
         ::   ::  ::   ::
        ::::  ::  ::  ::::
         ::   ::  ::   ::
             .::  ::.
             .::..::.
              ::::::
              ::  ::

Copyright © ${new Date().getFullYear()} Christian Care Ministry`;

export const initializeServer = (app: any) => {
  console.info(
    `------------------------------------------------------------\n` + 'Initializing server...',
  );

  /* Server Init */
  const normalizePort = (val = '3000') => {
    const port = parseInt(val, 10);

    if (Number.isNaN(port)) {
      // named pipe
      return val;
    }

    if (port >= 0) {
      // port number
      return port;
    }

    return false;
  };

  const port = normalizePort(Env.port);

  const server = (() => {
    return http.createServer(app);
  })();

  server.on('error', (error: any) => {
    if (error.syscall !== 'listen') {
      throw error;
    }

    const bind = typeof port === 'string' ? `Pipe ${port}` : `Port ${port}`;

    // handle specific listen errors with friendly messages
    switch (error.code) {
      case 'EACCES':
        console.error(`${bind} requires elevated privileges`);
        process.exit(1);
        break;
      case 'EADDRINUSE':
        console.error(`${bind} is already in use`);
        process.exit(1);
        break;
      default:
        throw error;
    }
  });

  server.on('listening', () => {
    const addr = server.address();
    const bind = typeof addr === 'string' ? addr : addr?.port;
    const message =
      `${asciiLogo}\n` +
      `------------------------------------------------------------\n` +
      `Server listening at http://localhost:${bind}\n` +
      `------------------------------------------------------------`;

    console.info(message);
  });

  // start server
  server.listen(port);
};

initializeServer(app);

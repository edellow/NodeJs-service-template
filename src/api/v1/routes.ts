// setup routes
import express from 'express';
import * as healthMonitorController from './controllers/healthMonitorController';
import * as mathController from './controllers/mathController';

const router = express.Router();

router.get('/ping', healthMonitorController.ping);

router.get('/calculations/add',      mathController.add);
router.get('/calculations/subtract', mathController.subtract);
router.get('/calculations/multiply', mathController.multiply);
router.get('/calculations/divide',   mathController.divide);

export default router;

import { StatusCodes } from 'http-status-codes';
import request from 'supertest';
import app from '../../../app';

describe('health Monitor Service', () => {
  test('should return ping', async () => {
    const response = await request(app).get('/api/v1/ping').send();
    expect(response.status).toBe(StatusCodes.OK);
    expect(response.text).toEqual('"pong"');
  });
});

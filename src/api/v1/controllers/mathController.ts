import { StatusCodes } from 'http-status-codes';
import validator from 'validator';
import { MathParams } from '../../../models/mathModels';
import {
  addService,
  divideService,
  multiplyService,
  subtractService,
} from '../../../services/mathService';

const validateMathParams = (data: any = {}) => {
  const errors = [];

  if (data.value1 === undefined || !validator.isNumeric(data.value1)) {
    errors.push({ value1: 'invalid or missing' });
  }
  if (data.value2 === undefined || !validator.isNumeric(data.value2)) {
    errors.push({ value2: 'invalid or missing' });
  }
  return errors.length ? errors : null;
};
const createMathParms = (data: any = {}) => {
  const mathParams: MathParams = {
    value1: Number(data.value1),
    value2: Number(data.value2),
  };
  return mathParams;
};

export const add = async (req: any, res: any): Promise<void> => {
  const errors = validateMathParams(req.query);
  if (errors) {
    res.status(StatusCodes.BAD_REQUEST).json({ message: 'Bad Request', content: errors });
    return;
  }

  await addService(createMathParms(req.query))
    .then((value: number) => {
      res.status(StatusCodes.OK).json({ value });
    })
    .catch(() => {
      res.status(StatusCodes.BAD_REQUEST).json(`Bad Request`);
    });
};

export const subtract = async (req: any, res: any): Promise<void> => {
  const errors = validateMathParams(req.query);
  if (errors) {
    res.status(StatusCodes.BAD_REQUEST).json({ message: 'Bad Request', content: errors });
    return;
  }

  await subtractService(createMathParms(req.query))
    .then((value: number) => {
      res.status(StatusCodes.OK).json({ value });
    })
    .catch(() => {
      res.status(StatusCodes.BAD_REQUEST).json(`Bad Request`);
    });
};

export const multiply = async (req: any, res: any): Promise<void> => {
  const errors = validateMathParams(req.query);
  if (errors) {
    res.status(StatusCodes.BAD_REQUEST).json({ message: 'Bad Request', content: errors });
    return;
  }

  await multiplyService(createMathParms(req.query))
    .then((value: number) => {
      res.status(StatusCodes.OK).json({ value });
    })
    .catch(() => {
      res.status(StatusCodes.BAD_REQUEST).json(`Bad Request`);
    });
};

export const divide = async (req: any, res: any): Promise<void> => {
  const errors = validateMathParams(req.query);
  if (errors) {
    res.status(StatusCodes.BAD_REQUEST).json({ message: 'Bad Request', content: errors });
    return;
  }

  await divideService(createMathParms(req.query))
    .then((value: number) => {
      res.status(StatusCodes.OK).json({ value });
    })
    .catch(() => {
      res.status(StatusCodes.BAD_REQUEST).json(`Bad Request`);
    });
};

import { StatusCodes } from 'http-status-codes';
import request from 'supertest';
import app from '../../../app';
import { MathParams } from '../../../models/mathModels';
import * as mathService from '../../../services/mathService';

// Mocking of math Service so that we are just testing the Math Controller and routing
jest.mock('../../../services/mathService');

describe('math service ', () => {
  // Add
  const mockAddService = mathService.addService as jest.Mock;
  mockAddService.mockImplementation(async (mathParams: MathParams): Promise<number> => {
    if (Number(mathParams.value1) === 999 || Number(mathParams.value2) === 999) {
      return Promise.reject(); // Fake something went wrong
    }
    return 99;
  });
  test('add should return 99', async () => {
    const response = await request(app).get('/api/v1/calculations/add?value1=99&value2=88').send();
    expect(response.status).toBe(StatusCodes.OK);
    expect(response.text).toBe('{"value":99}');
    expect(mockAddService).toHaveBeenCalledWith({ value1: 99, value2: 88 });
  });
  test('add should fail with 400-bad Request 1-fake', async () => {
    const response = await request(app)
      .get('/api/v1/calculations/add?value1=999&value2=888')
      .send();
    expect(response.status).toBe(StatusCodes.BAD_REQUEST);
    expect(mockAddService).toHaveBeenCalledWith({ value1: 999, value2: 888 });
  });
  test('add should fail with 400-bad Request 2-validate', async () => {
    const response = await request(app)
      .get('/api/v1/calculations/add?value1=niNe&value2=88')
      .send();
    expect(response.status).toBe(StatusCodes.BAD_REQUEST);
  });

  // Subtract
  const mockSubtractService = mathService.subtractService as jest.Mock;
  mockSubtractService.mockImplementation(async (mathParams: MathParams): Promise<number> => {
    if (Number(mathParams.value1) === 999 || Number(mathParams.value2) === 999) {
      return Promise.reject(); // Fake something went wrong
    }
    return 98;
  });
  test('subtract should return 98', async () => {
    const response = await request(app)
      .get('/api/v1/calculations/subtract?value1=100&value2=88.12345')
      .send();
    expect(response.status).toBe(StatusCodes.OK);
    expect(response.text).toBe('{"value":98}');
    expect(mockSubtractService).toHaveBeenCalledWith({
      value1: 100,
      value2: 88.12345,
    });
  });
  test('subtract should fail with 400-bad Request 1-fake', async () => {
    const response = await request(app)
      .get('/api/v1/calculations/subtract?value1=999&value2=0.999')
      .send();
    expect(response.status).toBe(StatusCodes.BAD_REQUEST);
    expect(mockSubtractService).toHaveBeenCalledWith({
      value1: 999,
      value2: 0.999,
    });
  });
  test('subtract should fail with 400-bad Request 2-validate', async () => {
    const response = await request(app)
      .get('/api/v1/calculations/subtract?value1=notAnumber&value2=88')
      .send();
    expect(response.status).toBe(StatusCodes.BAD_REQUEST);
  });

  // Divide
  const mockDivideService = mathService.divideService as jest.Mock;
  mockDivideService.mockImplementation(async (mathParams: MathParams): Promise<number> => {
    if (Number(mathParams.value1) === 999 || Number(mathParams.value2) === 999) {
      return Promise.reject(); // Fake something went wrong
    }
    return 97;
  });
  test('divide should return 97', async () => {
    const response = await request(app)
      .get('/api/v1/calculations/divide?value1=100&value2=88')
      .send();
    expect(response.status).toBe(StatusCodes.OK);
    expect(response.text).toBe('{"value":97}');
    expect(mockDivideService).toHaveBeenCalledWith({ value1: 100, value2: 88 });
  });
  test('divide should fail with 400-bad Request 1-fake', async () => {
    const response = await request(app)
      .get('/api/v1/calculations/divide?value1=999&value2=77.77777')
      .send();
    expect(response.status).toBe(StatusCodes.BAD_REQUEST);
    expect(mockDivideService).toHaveBeenCalledWith({
      value1: 999,
      value2: 77.77777,
    });
  });
  test('divide should fail with 400-bad Request 2-validate', async () => {
    const response = await request(app)
      .get('/api/v1/calculations/divide?value1=3&value2=8eight')
      .send();
    expect(response.status).toBe(StatusCodes.BAD_REQUEST);
  });

  // Multiply
  const mockMultiplyService = mathService.multiplyService as jest.Mock;
  mockMultiplyService.mockImplementation(async (mathParams: MathParams): Promise<number> => {
    if (Number(mathParams.value1) === 999 || Number(mathParams.value2) === 999) {
      return Promise.reject(); // Fake something went wrong
    }
    return 96;
  });
  test('multiple should return 96', async () => {
    const response = await request(app)
      .get('/api/v1/calculations/multiply?value1=100&value2=88')
      .send();
    expect(response.status).toBe(StatusCodes.OK);
    expect(response.text).toBe('{"value":96}');
    expect(mockMultiplyService).toHaveBeenCalledWith({
      value1: 100,
      value2: 88,
    });
  });
  test('multiply should fail with 400-bad Request 1-fake', async () => {
    const response = await request(app)
      .get('/api/v1/calculations/multiply?value1=999&value2=88')
      .send();
    expect(response.status).toBe(StatusCodes.BAD_REQUEST);
    expect(mockMultiplyService).toHaveBeenCalledWith({
      value1: 999,
      value2: 88,
    });
  });
  test('multiple should fail with 400-bad Request 2-validate', async () => {
    const response = await request(app)
      .get('/api/v1/calculations/multiply?value1=notAnumber&value2=88')
      .send();
    expect(response.status).toBe(StatusCodes.BAD_REQUEST);
  });
});

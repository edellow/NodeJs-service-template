import { Request, Response } from 'express';
import { StatusCodes } from 'http-status-codes';
import { pingService } from '../../../services/healthMonitorService';

export const ping = async (req: Request, res: Response): Promise<void> => {
  await pingService()
    .then((value: string) => {
      res.status(StatusCodes.OK).json(value);
    })
    .catch(() => {
      res.status(StatusCodes.BAD_REQUEST).json(`Bad Request`);
    });
};

/* eslint-disable no-process-env */
import { Env } from './env';
import { setupValidEnv } from './test/fixtures/env';

setupValidEnv();

describe(nameof(Env), () => {
  test('validate node environment', () => {
    process.env.NODE_ENV = 'Cool Environment';
    expect(() => Env.nodeEnv).toThrow('Wrong value passed for NODE_ENV.');

    process.env.NODE_ENV = 'test';
    expect(Env.nodeEnv).toEqual('test');
  });

  test('should return the port', () => {
    expect(Env.port).toEqual('3000');
  });

  test('validate debug mode', () => {
    process.env.DEBUG_MODE = 'false';
    expect(Env.debugMode).toEqual(false);

    process.env.DEBUG_MODE = 'true';
    expect(Env.debugMode).toEqual(true);
  });

  test('validate databaseURL', () => {
    expect(Env.databaseURL).toBe('postgres://127.0.0.1:5432/sample_test_service_dev');

    process.env.DATABASE_URL = '';
    process.env.TEST_DATABASE_URL = '';
    expect(() => Env.databaseURL).toThrow('Missing environment variable for TEST_DATABASE_URL');

    process.env.NODE_ENV = 'production';
    expect(() => Env.databaseURL).toThrow('Missing environment variable for DATABASE_URL');

    process.env.NODE_ENV = 'test';
    process.env.DATABASE_URL = 'fake_URL';
    process.env.TEST_DATABASE_URL = 'fake_URL';
    expect(Env.databaseURL).toEqual('fake_URL');
  });

  test('validate databaseUseSSL', () => {
    process.env.DATABASE_USE_SSL = 'true';
    expect(Env.databaseUseSSL).toEqual(true);

    process.env.DATABASE_USE_SSL = 'true';
    expect(Env.databaseUseSSL).toEqual(true);

    process.env.DATABASE_USE_SSL = '';
    expect(() => Env.databaseUseSSL).toThrow('Missing environment variable for DATABASE_USE_SSL');
  });

  test('validate corsOrigins', () => {
    process.env.CORS_ORIGINS_WHITELIST = 'A_value';
    expect(Env.corsOrigins).toEqual('A_value');
  });

  test('validate corsOriginsMatch', () => {
    process.env.CORS_ORIGINS_MATCH = 'A_value';
    expect(Env.corsOriginsMatch).toEqual('A_value');
  });

  test('validate accessTokenSecret', () => {
    process.env.ACCESS_TOKEN_SECRET = 'A_value';
    expect(Env.accessTokenSecret).toEqual('A_value');
    process.env.ACCESS_TOKEN_SECRET = '';
    expect(() => Env.accessTokenSecret).toThrow(
      'Missing environment variable for ACCESS_TOKEN_SECRET',
    );
  });

  test('validate swagger HostName', () => {
    expect(Env.swaggerHostname).toBe('http://localhost:4000/swagger');
  });
});

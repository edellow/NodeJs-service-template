import newrelicFormatter from '@newrelic/winston-enricher';
import expressWinston from 'express-winston';
import winston, { format } from 'winston';

// https://github.com/winstonjs/winston

const { printf, timestamp, label } = format;
const ccmFormat = printf(({ level, message, label, timestamp }) => {
  return `${timestamp} [${label}] ${level}: ${message}`;
});

// Ignore log messages
// Ignore if they have { private: true }
// This is a useful method so no sensitive data is logged
const ignorePrivate = format(info => {
  if (info.private) {
    return false;
  }
  return info;
});

const options = {
  fileInfo: {
    filename: 'logs/info.log',
    format: winston.format.combine(
      ignorePrivate(),
      newrelicFormatter(),
      label({ label: 'winstonLogger' }),
      timestamp(),
      ccmFormat,
    ),
    handleExceptions: true,
    json: true,
    level: 'info',
    maxFiles: 5,
    maxSize: 5 * 1024 * 1024, // 5 MB
  },
  fileError: {
    filename: 'logs/error.log',
    format: winston.format.combine(
      ignorePrivate(),
      label({ label: 'winstonLogger' }),
      timestamp(),
      ccmFormat,
    ),
    handleExceptions: true,
    json: true,
    level: 'error',
    maxFiles: 5,
    maxSize: 5 * 1024 * 1024, // 5 MB
  },
  consoleError: {
    colorize: true,
    format: winston.format.combine(
      ignorePrivate(),
      winston.format.prettyPrint(),
      winston.format.simple(),
    ),
    handleExceptions: true,
    level: 'error',
  },
};

const loggerInfo = winston.createLogger({
  defaultMeta: { service: 'sharing-service' },
  transports: [new winston.transports.File(options.fileInfo)],
  exitOnError: false, // do not exit on handled exceptions
});

const loggerError = winston.createLogger({
  defaultMeta: { service: 'sharing-service' },
  transports: [
    new winston.transports.File(options.fileError),
    new winston.transports.Console(options.consoleError),
  ],
  exitOnError: false, // do not exit on handled exceptions
});

export const loggerMiddleware = expressWinston.logger({
  winstonInstance: loggerInfo,
  colorize: false,
});

export const errorLoggerMiddleware = expressWinston.errorLogger({
  winstonInstance: loggerError,
});

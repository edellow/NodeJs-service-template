export const setupValidEnv = (): void => {
  process.env.NODE_ENV = 'test';
  process.env.PORT = '3000';
  process.env.DEBUG_MODE = 'false';

  process.env.DATABASE_URL = 'postgres://127.0.0.1:5432/sample_service_dev';
  process.env.TEST_DATABASE_URL = 'postgres://127.0.0.1:5432/sample_test_service_dev';
  process.env.DATABASE_USE_SSL = 'true';

  process.env.CORS_ORIGINS_WHITELIST = 'http://localhost:3000';
  process.env.CORS_ORIGINS_MATCH = 'http://localhost:4000';
  process.env.ACCESS_TOKEN_SECRET = 'fake_secret';

  process.env.CCM_CORE_API_BASE_URL = 'https://ccm-core-test.herokuapp.com';

  process.env.SWAGGER_HOSTNAME = 'http://localhost:4000/swagger';
};

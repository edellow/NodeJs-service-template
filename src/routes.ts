import router from './api/v1/routes';

// setup routes
interface RouteItem {
  version: string;
  config: any;
}
const routes: RouteItem[] = [{ version: 'v1', config: router }];

export const configureRoutes = (app: any): void => {
  routes.forEach((route: RouteItem) => {
    const routePath = `/api/${route.version}`;
    app.use(routePath, route.config);
  });
};
